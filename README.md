## Calendario
Código de prueba en PHP

## Descripción
Crear una función en PHP que reciba dos fechas como parámetros (fecha inicial y
fecha final), la función debe mostrar en pantalla cada día hábil, es decir de Lunes
a Viernes en formato “Human Readable”.

## Ejemplo
“Martes 5 de Septiembre de 2021” (no importa que sea en inglés).

## Consideraciones
Debe regresar el número total de días hábiles (que no sean ni sábado ni
domingo únicamente).

## Instalación
Abrir archivo "index.php" en un navegador web y ejecutar el servidor local

## Uso
Acceder fecha inicial y fecha final

## Autor
Francisco Saul Valle Nieto