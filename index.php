<!doctype html>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Calendario</title>
  </head>
  <body>

  <?php
      // Se incluye clase Calendar que nos ayudará a llevar a cabo la conversión de las fechas
      require_once("Calendar.php");


      //Se inicializa clase Calendar y fechas
      $calendar = new Calendar();
      $initialDate = null;
      $finalDate = null;

      // Evalua si el request incluye parametros requeridos
      if(
          isset($_REQUEST["initial-date"]) &&
          isset($_REQUEST["final-date"])
      ){
          $initialDate = $_REQUEST["initial-date"];
          $finalDate = $_REQUEST["final-date"];

          // Se establecen los parametros requeridos Fecha Inicial y Facha Final respectivamente
          $calendar->setDates($initialDate,$finalDate);

          // Retorna un arreglo de las fechas con los días hábiles
          $dates = $calendar->getList();
      }
  ?>

    <div class="container" id="wrapper">

        <!-- Page content wrapper-->
        <div id="page-content-wrapper">
            <!-- Top navigation-->
            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <div class="container-fluid">
                    <h1 class="mt-4">Calendario</h1>
                </div>
            </nav>

            <!-- Page content-->
            <div class="container-fluid">
                <h4 class="mt-4">Accede dos fechas</h4>


                <!-- Formulario de fechas -->
                <form action="index.php" method="POST">
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <label for="initial-date-field" class="form-label">Fecha Inicial</label>
                            <input type="date" name="initial-date" id="initial-date-field" class="form-control" value="<?php echo( $initialDate ) ?>" required>
                        </div>
                        <div class="col-sm-6 col-12">
                            <label for="final-date-field" class="form-label">Fecha Final</label>
                            <input type="date" name="final-date" id="final-date-field" class="form-control" value="<?php echo( $finalDate ) ?>" required>
                        </div>
                        <div class="col text-end py-3">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </form>


                <!-- Tabla de fechas -->
                <?php if( $calendar->is_set() ) { ?>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Día</th>
                            <th scope="col">Fecha</th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($dates as $index => $date) { ?>
                        <tr>
                            <th scope="row"><?php echo( $index ) ?></th>
                            <td><?php echo( $date["day"] ) ?></td>
                            <td><?php echo( $date["readable"] ) ?></td>
                        </tr>
                        <?php } //Fin Foreach?>
                        </tbody>
                    </table>
                <?php } //Fin If ?>

            </div>
        </div>
    </div>




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
-->
  </body>
</html>