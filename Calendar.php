<?php


class Calendar
{
    private $initialDate = null;
    private $finalDate = null;
    private $dayOff = ['Saturday','Sunday'];   // Días de descanso

    /*
     * Inicializar fechas
     * Recibe dos parametros de inicialización
     *
     * @param String $initialDate
     * @param String $finalDate
     */
    public function setDates($initialDate, $finalDate)
    {
        $this->initialDate = $initialDate;
        $this->finalDate = $finalDate;
    }


    /*
     * Valida si han sido inicializadas las fechas
     * @return boolean
     */
    public function is_set(){
        if(is_null($this->initialDate))
            return false;

        if(is_null($this->finalDate))
            return false;

        return true;
    }


    /*
     * Método Index
     * Retorna un arreglo de las fechas con los días hábiles
     *
     * @return array()
     */
    public function getList() {
        $dates = array();

        //Intervalo de dias P1D: 1 día
        $interval = new DateInterval('P1D');

        // Objeto de fecha inicial y final
        $firstDate = new DateTime($this->initialDate);
        $lastDate = new DateTime($this->finalDate);

        // Tiemstamp de fecha inicial y final
        $firstTimestamp = $firstDate->getTimestamp();
        $lastTimestamp = $lastDate->getTimestamp();


        // Mientras fecha inicial sea menor o igual que fecha final
        // le sumará un día y lo guardará en el arreglo
        while($firstTimestamp <= $lastTimestamp) {
            $day = date("l", $firstTimestamp);
            $readable = strftime('%A %e %B %Y', $firstDate->getTimestamp());

            $date = [
                "day"      => $day,
                "readable"  => $readable
            ];

            // Descarta días libres
            if($this->validateDayOff($day)){
                array_push($dates, $date);
            }

            // Suma 1 día a la fecha de inicio
            $firstDate->add($interval);
            $firstTimestamp = $firstDate->getTimestamp();
        }

        return $dates;

    }


    /*
     * Función auxiliar para validar los días libres
     *
     * @param String $day
     */
    private function validateDayOff($day){
        foreach ($this->dayOff as $dayOff){
                if($dayOff === $day)
                    return false;
        }

        return true;
    }

}